package ru.kiselev.tm.taskmanager.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.kiselev.tm.taskmanager.entity.Project;
import ru.kiselev.tm.taskmanager.repository.ProjectRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@RequiredArgsConstructor
public class ProjectService {
    private final ProjectRepository projectRepository;

    public List<Project> getProjectsList() {
        Iterable<Project> all = projectRepository.findAll();
        return StreamSupport.stream(all.spliterator(), false)
                .collect(Collectors.toList());
    }

    public Project showProject(long id) {
        return projectRepository.findById(id).orElseThrow(() -> new RuntimeException("Not found project by id: " + id));
    }

    public void saveProject(Project project) {
        projectRepository.save(project);
    }

    public void updateProject(Project updatedProject) {
        long id = updatedProject.getId();
        if (projectRepository.existsById(id)) {
            projectRepository.save(updatedProject);
        } else
            new RuntimeException("Not found project by id: " + id);
    }

    public void deleteProject(long id) {
        projectRepository.deleteById(id);
    }
}
