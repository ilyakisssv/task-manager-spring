package ru.kiselev.tm.taskmanager.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.kiselev.tm.taskmanager.entity.Task;
import ru.kiselev.tm.taskmanager.repository.TaskRepository;

import java.util.List;

@Component
@RequiredArgsConstructor
public class TaskService {
    private final TaskRepository taskRepository;

    public List<Task> index() {
        return (List<Task>) taskRepository.findAll();
    }

    public Task show(long id) {
        return taskRepository.findById(id).orElseThrow(() -> new RuntimeException("Not found project by id: " + id));
    }

    public void save(Task task) {
        taskRepository.save(task);
    }

    public void update(Task updatedTask) {
        long id = updatedTask.getId();
        if (taskRepository.existsById(id)) {
            taskRepository.save(updatedTask);
        } else
            new RuntimeException("Not found project by id: " + id);
    }

    public void delete(long id) {
        taskRepository.deleteById(id);
    }
}
