package ru.kiselev.tm.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.kiselev.tm.taskmanager.entity.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {
}
