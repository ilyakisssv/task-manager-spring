package ru.kiselev.tm.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.kiselev.tm.taskmanager.entity.Project;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {
}
