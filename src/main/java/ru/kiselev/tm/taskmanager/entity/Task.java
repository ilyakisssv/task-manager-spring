package ru.kiselev.tm.taskmanager.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "TASKS")
public class Task {
    @Id
    @GeneratedValue
    private long id;
    //private long idUser;

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @NotEmpty(message = "Don't is empty!")
    private String name;
    @NotEmpty(message = "Don't is empty!")
    private String description;
    @DateTimeFormat(style = "dd-MM-yyyy")
    private LocalDate dateFrom;
    @DateTimeFormat(style = "dd-MM-yyyy")
    private LocalDate dateTo;
    private Status status;

    //    @Override
//    public int compareTo(Task o) {
//        return Long.compare(id, o.getId());
//    }

    @Override
    public String toString() {
        return "Project" +
                " id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", status=" + status;
    }
}
