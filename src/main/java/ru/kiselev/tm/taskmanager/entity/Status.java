package ru.kiselev.tm.taskmanager.entity;

public enum Status {
    PLANNED("Запланировано"),
    IN_PROGRESS("В процессе"),
    READY("Готово");

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }
}
