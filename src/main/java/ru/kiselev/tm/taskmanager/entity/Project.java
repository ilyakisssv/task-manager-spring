package ru.kiselev.tm.taskmanager.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "PROJECTS")
public class Project {
    @Id
    @GeneratedValue
    private long id;
    //private long idUser;

    @NotEmpty(message = "Don't is empty!")
    private String name;
    @NotEmpty(message = "Don't is empty!")
    private String description;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate dateFrom;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate dateTo;
    private Status status;

    @OneToMany(mappedBy = "project")
    private List<Task> taskList;


    @Override
    public String toString() {
        return "Project" +
                " id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", status=" + status;
    }
}
