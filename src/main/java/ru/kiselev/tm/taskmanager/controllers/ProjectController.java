package ru.kiselev.tm.taskmanager.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.kiselev.tm.taskmanager.entity.Project;
import ru.kiselev.tm.taskmanager.service.ProjectService;

import javax.validation.Valid;

@Controller
@RequestMapping("/projects")
@RequiredArgsConstructor
public class ProjectController {
    private final ProjectService projectService;

    @GetMapping()
    public String index(Model model) {
        model.addAttribute("project", projectService.getProjectsList());
        return "projects/index";
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") long id, Model model) {
        model.addAttribute("project", projectService.showProject(id));
        return "projects/show";
    }

    @GetMapping("/new")
    public String newProject(Model model) {
        model.addAttribute("project", new Project());
        return "projects/new";
    }

    @PostMapping()
    public String create(@ModelAttribute("project") @Valid Project project, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "projects/new";
        }
        projectService.saveProject(project);
        return "redirect:/projects";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") long id) {
        model.addAttribute("project", projectService.showProject(id));
        return "projects/edit";
    }

    @PatchMapping("/{id}")
    public String update(@ModelAttribute("project") @Valid Project project, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "projects/edit";
        }
        return "redirect:/projects";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") long id) {
        projectService.deleteProject(id);
        return "redirect:/projects";
    }
}
