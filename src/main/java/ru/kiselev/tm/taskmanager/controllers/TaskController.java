package ru.kiselev.tm.taskmanager.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.kiselev.tm.taskmanager.service.TaskService;
import ru.kiselev.tm.taskmanager.entity.Task;

import javax.validation.Valid;

@Controller
@RequestMapping("/projects/{id}/tasks")
public class TaskController {
    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskDao) {
        this.taskService = taskDao;
    }

    public String index(Model model) {
        model.addAttribute("task", taskService.index());
        return "/projects/{id}/tasks/index";
    }

    @GetMapping("/{idT}")
    public String show(@PathVariable("idT") long id, Model model) {
        model.addAttribute("task", taskService.show(id));
        return "/projects/{id}/tasks/show";
    }

    @PostMapping
    public String create(@ModelAttribute("task") Task task, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "/projects/{id}/tasks/new";
        }
        taskService.save(task);
        return "redirect:/projects/{id}/tasks";
    }

    @GetMapping("/new")
    public String newTask(Model model) {
        model.addAttribute("task", new Task());
        return "redirect:/projects/{id}/tasks";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") long id) {
        model.addAttribute("task", taskService.show(id));
        return "/projects/{id}/tasks/edit";
    }

    @PatchMapping("/{id}")
    public String update(@ModelAttribute("task") @Valid Task task, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "/projects/{id}/tasks/edit";
        }
        return "redirect:/projects/{id}/tasks";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") long id) {
        taskService.delete(id);
        return "redirect:/projects/{id}/tasks";
    }
}
